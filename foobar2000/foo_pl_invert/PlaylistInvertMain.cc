#include <SDK/foobar2000.h>

// {80821CA9-F411-4B24-8D69-E20C8743B029}
static GUID const g_playlistInvertGuid = {
    0x80821ca9,
    0xf411,
    0x4b24,
    { 0x8d, 0x69, 0xe2, 0xc, 0x87, 0x43, 0xb0, 0x29 }
};

struct PlaylistInvertMenuCommands : mainmenu_commands
{
    virtual t_uint32 get_command_count() override { return 1; }
    virtual GUID get_command(t_uint32 p_index) override
    {
        return g_playlistInvertGuid;
    }
    virtual void get_name(t_uint32 p_index, pfc::string_base& p_out) override
    {
        p_out = "Invert";
    }
    virtual bool get_description(t_uint32 p_index,
                                 pfc::string_base& p_out) override
    {
        p_out =
          "Invert selection of items in the list to all non-selected items.";
        return true;
    }
    virtual GUID get_parent() override
    {
        return mainmenu_groups::edit_part2_selection;
    }
    virtual void execute(t_uint32 p_index,
                         service_ptr_t<service_base> p_callback) override
    {
        service_ptr_t<playlist_manager> pm;
        if (!playlist_manager::tryGet(pm)) {
            console::warning(
              "[foo_pl_invert] Cannot acquire playlist manager.");
            return;
        }
        size_t n = pm->activeplaylist_get_item_count();
        pfc::bit_array_bittable selection{ n };
        pm->activeplaylist_get_selection_mask(selection);
        pm->activeplaylist_set_selection(pfc::bit_array_true{}, pfc::bit_array_not{selection});
    }
};

static mainmenu_commands_factory_t<PlaylistInvertMenuCommands> g_plimc;

DECLARE_COMPONENT_VERSION("Playlist selection invert", "1.1", "zao")
VALIDATE_COMPONENT_FILENAME("foo_pl_invert.dll")
